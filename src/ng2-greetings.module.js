import { NgModule } from '@angular/core';
import { Ng2Greetings } from './ng2-greetings';
export function Ng2GreetingsFactory() {
    return new Ng2Greetings();
}
;
export var Ng2GreetingsModule = (function () {
    function Ng2GreetingsModule() {
    }
    Ng2GreetingsModule.decorators = [
        { type: NgModule, args: [{
                    providers: [
                        {
                            provide: Ng2Greetings,
                            useFactory: Ng2GreetingsFactory
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    Ng2GreetingsModule.ctorParameters = function () { return []; };
    return Ng2GreetingsModule;
}());
//# sourceMappingURL=ng2-greetings.module.js.map