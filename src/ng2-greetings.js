import { Injectable } from '@angular/core';
export var Ng2Greetings = (function () {
    function Ng2Greetings() {
    }
    Ng2Greetings.prototype.greetUser = function () {
        return "Hello World";
    };
    Ng2Greetings.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    Ng2Greetings.ctorParameters = function () { return []; };
    return Ng2Greetings;
}());
//# sourceMappingURL=ng2-greetings.js.map