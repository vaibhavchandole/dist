(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core'], factory) :
    (factory((global['ng2-greetings'] = global['ng2-greetings'] || {}),global.ng.core));
}(this, (function (exports,_angular_core) { 'use strict';

var Ng2Greetings = (function () {
    function Ng2Greetings() {
    }
    Ng2Greetings.prototype.greetUser = function () {
        return "Hello World";
    };
    Ng2Greetings.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    Ng2Greetings.ctorParameters = function () { return []; };
    return Ng2Greetings;
}());

function Ng2GreetingsFactory() {
    return new Ng2Greetings();
}

var Ng2GreetingsModule = (function () {
    function Ng2GreetingsModule() {
    }
    Ng2GreetingsModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    providers: [
                        {
                            provide: Ng2Greetings,
                            useFactory: Ng2GreetingsFactory
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    Ng2GreetingsModule.ctorParameters = function () { return []; };
    return Ng2GreetingsModule;
}());

exports.Ng2GreetingsModule = Ng2GreetingsModule;
exports.Ng2Greetings = Ng2Greetings;

Object.defineProperty(exports, '__esModule', { value: true });

})));
